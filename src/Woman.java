import java.util.Map;

public final class Woman extends Human{
    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, Map<DayOfWeek, Activity> schedule, Family family) {
        super(name, surname, year, iq, schedule, family);
    }

    public Woman() {
    }

    void makeup(){
        System.out.println("Підфарбуватися");
    }

    /*@Override
    public void greetPet() {
        System.out.println("Привіт, " + getFamily().getPet().getNickname() + ", ти така мила киця!");
    }*/

    @Override
    public void greetPet() {
        super.greetPet();
    }
}
