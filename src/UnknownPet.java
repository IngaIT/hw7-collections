import java.util.Set;

public class UnknownPet extends Pet {
        public UnknownPet(String nickname) {
            super(nickname);
            setSpecies(Species.UNKNOWN);
        }

    public UnknownPet(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public UnknownPet() {
    }
}

