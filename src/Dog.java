import java.util.Set;

public class Dog extends Pet implements Foulable{
    public Dog(String nickname){
        super(nickname);
        setSpecies(Species.DOG);
    }

    public Dog(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public Dog() {
    }

    public Dog(String барбос, String buddy, int i, Set<String> dogHabits) {
    }

    @Override
    public void respond() {
        System.out.println("Привіт, я собака " + this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Я собака і я роблю гидоти...");
    }
}