
import java.util.*;

public class Main {
    public static void main(String[] args) {

        Set<String> dogHabits = new HashSet<>(Arrays.asList("їсть", "грається", "спить"));
        Dog dog = new Dog("Барбос", "Buddy", 75, dogHabits);

        Set<String> catHabits = new HashSet<>();
        DomesticCat cat = new DomesticCat( Pet.Species.CAT, "Мурзік", 2, 40, catHabits);

        Map<Human.DayOfWeek, Human.Activity> father1Schedule = new HashMap<Human.DayOfWeek, Human.Activity>();
        father1Schedule.put(Human.DayOfWeek.MONDAY, Human.Activity.WORK);
        father1Schedule.put(Human.DayOfWeek.TUESDAY, Human.Activity.WORK);
        father1Schedule.put(Human.DayOfWeek.FRIDAY, Human.Activity.THEATRE);
        father1Schedule.put(Human.DayOfWeek.SATURDAY, Human.Activity.SWIMMING);
        Human father1 = new Human("Олег", "Іванов", 1975, 130, father1Schedule);

        Human mother1 = new Human("Олена", "Іванова", 1977);
        Human child1 = new Human("Марія", "Іванова", 2001);

        Human father2 = new Human("Андрій", "Петров", 1980);
        Human mother2 = new Human("Наталія", "Петрова", 1982);

        Map<Human.DayOfWeek, Human.Activity> child2Schedule = new HashMap<>();
        child2Schedule.put(Human.DayOfWeek.MONDAY, Human.Activity.WORK);
        child2Schedule.put(Human.DayOfWeek.WEDNESDAY, Human.Activity.STUDY);
        child2Schedule.put(Human.DayOfWeek.THURSDAY, Human.Activity.CINEMA);
        child2Schedule.put(Human.DayOfWeek.SUNDAY, Human.Activity.SLEEP);
        Human child2 = new Human("Максим", "Петров", 2005, 100, child2Schedule);

        Family family1 = new Family(mother1, father1);
        family1.addPet(dog);
        family1.addChild(child1);

        Family family2 = new Family(mother2, father2);
        family2.addPet(cat);
        family2.addChild(child2);

        System.out.println(family1);
        System.out.println(family2);

        child1.greetPet();
        child1.describePet();

        child2.greetPet();
        child2.describePet();

        dog.eat();
        dog.respond();
        dog.foul();

        cat.eat();
        cat.respond();
        cat.foul();

        System.out.println(family1.countFamily());
        System.out.println(family2.countFamily());

        family1.deleteChild(0);
        family2.deleteChild(0);

        System.out.println(family1);
        System.out.println(family2);

        System.out.println(family1.countFamily());
        System.out.println(family2.countFamily());
    }
}
