import java.util.Set;

public class RoboCat extends Pet implements Foulable{
    public RoboCat(String nickname) {
        super(nickname);
        setSpecies(Species.RCAT);
    }

    public RoboCat(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public RoboCat() {
    }

    @Override
    public void respond() {
        System.out.println("Привіт, я робокіт " + this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Я робокіт і я роблю гидоти...");
    }
}